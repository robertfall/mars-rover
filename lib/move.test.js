// @flow
import { move as rawMove } from "./move";

const move = (rover) => rawMove(rover, { width: 8, height: 8 });

test("move moves the rover one step in the direction it is facing", () => {
  expect(move({ x: 1, y: 1, direction: "N" })).toEqual({
    x: 1,
    y: 2,
    direction: "N"
  });

  expect(move({ x: 1, y: 1, direction: "S" })).toEqual({
    x: 1,
    y: 0,
    direction: "S"
  });

  expect(move({ x: 1, y: 1, direction: "E" })).toEqual({
    x: 2,
    y: 1,
    direction: "E"
  });

  expect(move({ x: 1, y: 1, direction: "W" })).toEqual({
    x: 0,
    y: 1,
    direction: "W"
  });
});

test("move is composable", () => {
  expect(move(move({ x: 1, y: 1, direction: "E" }))).toEqual({
    x: 3,
    y: 1,
    direction: "E"
  })
})

test("throws an error if the move would put the rover in danger", () => {
  expect(() => move({ x: 1, y: 8, direction: "N" })).toThrow(
    "Attempted move would put rover in unsafe area: {1,8}"
  );
  expect(() => move({ x: 1, y: 0, direction: "S" })).toThrow(
    "Attempted move would put rover in unsafe area: {1,0}"
  );
  expect(() => move({ x: 8, y: 1, direction: "E" })).toThrow(
    "Attempted move would put rover in unsafe area: {8,1}"
  );
  expect(() => move({ x: 0, y: 1, direction: "W" })).toThrow(
    "Attempted move would put rover in unsafe area: {0,1}"
  );
});
