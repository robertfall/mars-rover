// @flow
import type { CardinalPoint } from './types';

/**
 * Rotates a cardinal direction 90° Clockwise
 * 
 * @param {CardinalPoint} The current direction 
 * @returns {CardinalPoint} The rotated direction
 */
export function rotateClockwise(currentDirection: CardinalPoint): CardinalPoint {
  switch (currentDirection) {
    case "N":
      return "E";
    case "E":
      return "S";
    case "S":
      return "W";
    case "W":
      return "N";
    default:
      throw new Error(
        `Attempted to rotate from unknown cardinal point: ${currentDirection}`
      );
  }
}

/**
 * Rotates a cardinal direction 90° Counterclockwise
 * 
 * @param {CardinalPoint} The current direction 
 * @returns {CardinalPoint} The rotated direction
 */
export function rotateCounterclockwise(
  currentDirection: CardinalPoint
): CardinalPoint {
  switch (currentDirection) {
    case "N":
      return "W";
    case "E":
      return "N";
    case "S":
      return "E";
    case "W":
      return "S";
    default:
      throw new Error(
        `Attempted to rotate from unknown cardinal point: ${currentDirection}`
      );
  }
}