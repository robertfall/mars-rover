const coordinatesRegex = /^\d{1,} \d{1,}$/
const roverRegex = /^\d{1,} \d{1,} [NESW]$/
const movesRegex = /^[LRM]+$/

export function isValid(content) {
  const lines = content.split(/\r?\n\r?\n/)

  if (lines.length !== 3) { return false; }

  const [grid, rover, instructions] = lines;

  if (!coordinatesRegex.test(grid)) { return false; }
  if (!roverRegex.test(rover)) { return false; }
  if (!movesRegex.test(instructions)) { return false; }

  return true;
}