// @flow
import { rotateClockwise, rotateCounterclockwise } from "./rotate";

test("clockwise rotation works", () => {
  expect(rotateClockwise("N")).toBe("E");
  expect(rotateClockwise("E")).toBe("S");
  expect(rotateClockwise("S")).toBe("W");
  expect(rotateClockwise("W")).toBe("N");
});

test("clockwise rotation is composable", () => {
  expect(rotateClockwise(rotateClockwise("N"))).toBe("S");
});

test("clockwise rotation throws error if cardinal point is not recognised", () => {
  expect(() =>
    rotateClockwise("UNKNOWN")
  ).toThrow("Attempted to rotate from unknown cardinal point: UNKNOWN");
});

test("counterclockwise rotation works", () => {
  expect(rotateCounterclockwise("N")).toBe("W");
  expect(rotateCounterclockwise("E")).toBe("N");
  expect(rotateCounterclockwise("S")).toBe("E");
  expect(rotateCounterclockwise("W")).toBe("S");
});

test("counterclockwise rotation is composable", () => {
  expect(rotateCounterclockwise(rotateCounterclockwise("N"))).toBe("S");
});

test("counterclockwise rotation throws error if cardinal point is not recognised", () => {
  expect(() =>
    rotateCounterclockwise(
      "UNKNOWN"
    )
  ).toThrow("Attempted to rotate from unknown cardinal point: UNKNOWN");
});
