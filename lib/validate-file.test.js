// @flow

import { isValid } from "./validate-file";

test("returns true if file is valid", () => {
  expect(isValid("8 8\n\n1 2 E\n\nMRMMLMRM")).toBe(true);
});

test("returns false if file is invalid", () => {
  expect(isValid(`8 8`)).toBe(false);

  expect(isValid("8 \n\n1 2\n\nMRMMLMRM")).toBe(false);
  expect(isValid("8a 8\n\n1 2\n\nMRMMLMRM")).toBe(false);
  expect(isValid("8 M\n\n1 2\n\nMRMMLMRM")).toBe(false);
  expect(isValid("8 8 9\n\n1 2\n\nMRMMLMRM")).toBe(false);

  expect(isValid("8 \n\n 2\n\nMRMMLMRM")).toBe(false);
  expect(isValid("8a 8\n\n1a 2\n\nMRMMLMRM")).toBe(false);
  expect(isValid("8 M\n\n1 B\n\nMRMMLMRM")).toBe(false);
  expect(isValid("8 M\n\n1 1 1\n\nMRMMLMRM")).toBe(false);

  expect(isValid("8 8\n\n1 2\n\nMRMMLMRA")).toBe(false);
  expect(isValid("8 8\n\n1 2\n\n")).toBe(false);
  expect(isValid("8 8\n\n1 2\n\n8 8")).toBe(false);
  expect(isValid("8 8\n\n1 2\n\nABC")).toBe(false);
});
