// @flow
export type Rotation = "R" | "L";
export type Move = "M";
export type CardinalPoint = "N" | "S" | "E" | "W";
export type RoverDescription = { x: number, y: number, direction: CardinalPoint };
export type Grid = { height: number, width: number };
export type Instruction = Rotation | Move;