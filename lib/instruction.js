// @flow
import type { RoverDescription, Instruction, Grid } from "./types";

import { rotateClockwise, rotateCounterclockwise } from "./rotate";
import { move } from "./move";

export function instructionFactory(grid: Grid) {
  return function applyInstruction(
    rover: RoverDescription,
    instruction: Instruction
  ) {
    switch (instruction) {
      case "R":
        return { ...rover, direction: rotateClockwise(rover.direction) };
      case "L":
        return { ...rover, direction: rotateCounterclockwise(rover.direction) };
      case "M":
        return move(rover, grid);
      default:
        throw new Error(`Unknown rover instruction ${instruction}`);
    }
  };
}
