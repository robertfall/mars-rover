// @flow

import { instructionFactory } from "./instruction";

// In this file I decided to try using Jest's snapshot testing
// which is quite handy for detecting regressions in higher level
// functions when changing any of it's children.

const grid = { width: 8, height: 8 };
const applyInstruction = instructionFactory(grid);

test("apply instruction works when called once", () => {
  const rover = { x: 0, y: 0, direction: "N" };

  expect(applyInstruction(rover, "R")).toMatchSnapshot();
  expect(applyInstruction(rover, "L")).toMatchSnapshot();
  expect(applyInstruction(rover, "M")).toMatchSnapshot();
});

test("apply instruction can be composed", () => {
  const rover = { x: 0, y: 0, direction: "N" };

  expect(applyInstruction(applyInstruction(rover, "M"), "L")).toMatchSnapshot();
});

test("apply instruction can be reduced", () => {
  const rover = { x: 0, y: 0, direction: "N" };
  const instructions = ["M", "M", "R", "M"];

  expect(instructions.reduce(applyInstruction, rover)).toMatchSnapshot();
});
