// @flow
import type { RoverDescription, Grid } from "./types";
/**
 * Moves the rover one step in the direction it's currently facing
 * 
 * @param {RoverDescription} The current rover state 
 * @returns {RoverDescription} The rover state after moving
 */
export function move(rover: RoverDescription, grid: Grid): RoverDescription {
  const { x, y, direction } = rover;
  let newRover;

  switch (direction) {
    case "N":
      newRover = { ...rover, y: y + 1 };
      break;
    case "E":
      newRover = { ...rover, x: x + 1 };
      break;
    case "S":
      newRover = { ...rover, y: y - 1 };
      break;
    case "W":
      newRover = { ...rover, x: x - 1 };
      break;
    default:
      throw new Error(
        `Attempted to rotate from unknown cardinal point: ${direction}`
      );
  }

  if (
    newRover.y < 0 ||
    newRover.y > grid.height ||
    newRover.x < 0 ||
    newRover.x > grid.width
  ) {
    throw new Error(
      `Attempted move would put rover in unsafe area: {${x},${y}}`
    );
  }
  return newRover;
}
