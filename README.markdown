# Mars Rover Simulator

This project was built targetting node v7.9.0 and was tested against Yarn and NPM.
Please make sure you version of node is up to date.

## Installing

Clone this repository onto your local machine: 
```sh
git clone git@gitlab.com:robertfall/mars-rover.git
```

Install dev dependencies:

```sh
npm install
```
or
```sh
yarn install
```

## Build 

```sh
npm run build
```
or
```sh
yarn build
```

## Usage

> Make sure to build the project before running

```sh
./rover [data file]
```

Test data is provided at `test-data/data.txt`.

```sh
./rover test-data/data.txt
```

If your node executable is not at the default path you may have to execute it manually:

```sh
node rover test-data/data.txt
```

## Design

I used a functional approach to solve this problem, breaking down each instruction into it's own pure function that was easily testable and then composing the smaller functions together into the application executable.

To ensure correctness I used a multi-layered approach to input verification using a combination of guard clauses and default cases and verified them with unit tests.

While the default cases in the switch statements should in theory be unreachable thanks to Flow's type safety the Flow compiler complained when I left them out and I thought adding the default cases would be safer if the code was called from untyped javascript.

I used this project as an opportunity to try Flow for type safety and Jest for simple testing. Flow's [union types](https://flow.org/en/docs/types/unions/) proved to be very powerful and the coding experience overall was very pleasant.

I made use of Jest's snapshot testing on the high level function as I think it's a low cost way to get a regression suite on complicated functions — simply provide scenarios, verify results and Jest will make sure the behaviour of the function doesn't change. This can be very useful when refactoring old code.

The executable itself is probably the least elegant of the code as I didn't spend too long on figuring out how to transpile an executable using Flow types and elected to rather write it in pure node Javascript.